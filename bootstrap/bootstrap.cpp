// bootstrap.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>
#include <conio.h>

#include <common/testmodule.h>
#include <common/renderermodule.h>
#include <common/api_registry.h>

#include "module_loader.h"

api_registry_i* get_api_registry(); // we forward declare this, find a proper way to make the getter available everywhere (even inside modules (tip: the registry is provided on loading))

#ifdef _WIN32
#include <Windows.h>
#endif

int main()
{
#ifdef _WIN32
  // chdir to exe path (TODO : clean this up)
  wchar_t exe_path[1024];
  if (GetModuleFileName(NULL, exe_path, 1024))
  {
    // remove filename from the path
    wchar_t* last_slash = wcsrchr(exe_path, '\\');
    if (last_slash != nullptr) {
      last_slash[1] = 0;
      // chdir to path
      SetCurrentDirectory(exe_path);
    }
  }
#endif

  const std::string module_names[] = {
    "testmodule",
    "python_plugin",
	"vkrenderer"
  };

  // load modules
  for (auto& name : module_names)
    add_module(name);

  if (!load_modules())
    std::cerr << "failed to load the modules" << std::endl;

  // fetch and use modules using test module interface
  int i = 0;
  for (
	  testmodule_i* module = (testmodule_i*)get_api_registry()->first(TESTMODULE_API_NAME);
	  module != nullptr;
	  module = (testmodule_i*)get_api_registry()->next(module)
	  )
  {
	  // print the value returned by the module
	  std::cout << "Module " << module->name << " (" << i++ << ") said " << module->return_something() << std::endl;
  }

  // fetch and use renderer module
  renderermodule_i* renderer = (renderermodule_i*)get_api_registry()->first(RENDERERMODULE_API_NAME);
  if (renderer == nullptr || !renderer->init())
  {
	  std::cerr << "Failed to init renderer module" << std::endl;
	  return 1;
  }

  while (true)
  {
    // check if the modules changed and reload them if necessary
    update_modules();

    // loop module
	renderer = (renderermodule_i*)get_api_registry()->first(RENDERERMODULE_API_NAME);
	if (renderer == nullptr || !renderer->loop())
		break;
  }

  // cleanup renderer
  renderer = (renderermodule_i*)get_api_registry()->first(RENDERERMODULE_API_NAME);
  if (renderer != nullptr)
	  renderer->cleanup();

  // Unload modules and quit
  if (!unload_modules())
    std::cerr << "failed to load the modules" << std::endl;
  else
    std::cout << "all modules should now be unloaded. (press any key to continue)" << std::endl;

  _getch();

  return 0;
}
