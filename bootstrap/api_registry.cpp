
#include "stdafx.h"

#include <common/api_registry.h>

#include <vector>
#include <map>
#include <string>
#include <iostream>

typedef std::vector<void*> Interfaces;
typedef std::map<std::string, Interfaces> InterfaceMap;

static InterfaceMap interfaces;

void addInterface(const char *name, void *interf)
{
  std::string str_name(name);
  auto it = interfaces.find(str_name);
  if (it == interfaces.end())
  {
    Interfaces val;
    interfaces.insert(std::pair<std::string, Interfaces>(str_name, val));
    it = interfaces.find(str_name);
  }

  // check no duplicates and insert
  auto& vec = it->second;
  if (std::find(vec.begin(), vec.end(), interf) == vec.end())
    vec.push_back(interf);
  else
    std::cerr << "Warning : interface was already registered" << std::endl;
}

void removeInterface(void *interf)
{
  for (auto it = interfaces.begin(); it != interfaces.end(); ++it)
  {
    Interfaces& val = it->second;
    auto it2 = std::find(val.begin(), val.end(), interf);
    if (it2 != val.end())
    {
      val.erase(it2);

      if (val.empty())
        interfaces.erase(it);
      return;
    }
  }
}

void* firstInterface(const char *name)
{
  std::string str_name(name);
  auto it = interfaces.find(str_name);
  if (it != interfaces.end() && !it->second.empty())
    return it->second.front();

  return nullptr;
}

void* nextInterface(void *prev)
{
  for (auto it = interfaces.begin(); it != interfaces.end(); ++it)
  {
    Interfaces& val = it->second;
    auto it2 = std::find(val.begin(), val.end(), prev);
    if (it2 != val.end())
    {
      it2++;

      if (it2 != val.end())
        return (*it2);
      else
        return nullptr;
    }
  }
  return nullptr;
}

static struct api_registry_i registry = {
  &addInterface,
  &removeInterface,
  &firstInterface,
  &nextInterface
};

api_registry_i* get_api_registry()
{
  return &registry;
}
