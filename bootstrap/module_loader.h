#pragma once

#include <string>

bool add_module(const std::string& module_name);
bool load_modules();
bool unload_modules();
bool update_modules();
