#include "stdafx.h"

#include "module_loader.h"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <sstream>
#include <vector>
#include <Windows.h>

#include <common/api_registry.h>

api_registry_i* get_api_registry(); // we forward declare this, find a proper way to make the getter available everywhere (even inside modules (tip: the registry is provided on loading))

typedef int(*LoadFn)(struct api_registry_i*, int);
typedef int(*UnLoadFn)(struct api_registry_i*, int);

struct Module
{
  std::string name;
  int loaded_revision;
  FILETIME loaded_filetime;
  bool loaded;
};

static std::vector<Module> modules;

static std::wstring convert_to_utf16(const std::string& str)
{
  static constexpr size_t max_str_len = 1024;
  wchar_t converted_str[max_str_len];
  size_t wlen = MultiByteToWideChar(CP_UTF8, MB_PRECOMPOSED, str.c_str(), -1, nullptr, 0);
  assert(wlen != 0 && wlen < max_str_len);
  MultiByteToWideChar(CP_UTF8, MB_PRECOMPOSED, str.c_str(), -1, converted_str, max_str_len);
  return std::wstring(converted_str);
}

static std::string convert_to_utf8(const std::wstring& str)
{
  static constexpr size_t max_str_len = 1024;
  char converted_str[max_str_len];
  size_t len = WideCharToMultiByte(CP_UTF8, 0, str.c_str(), -1, nullptr, 0, NULL, NULL);
  //size_t len = MultiByteToWideChar(CP_UTF8, MB_PRECOMPOSED, str.c_str(), -1, nullptr, 0);
  assert(len != 0 && len < max_str_len);
  WideCharToMultiByte(CP_UTF8, 0, str.c_str(), -1, converted_str, max_str_len, NULL, NULL);
  //MultiByteToWideChar(CP_UTF8, MB_PRECOMPOSED, str.c_str(), -1, converted_str, max_str_len);
  return std::string(converted_str);
}

static std::string get_last_error_string()
{
  DWORD error = GetLastError();
  if (error)
  {
    LPVOID lpMsgBuf;
    DWORD bufLen = FormatMessage(
      FORMAT_MESSAGE_ALLOCATE_BUFFER |
      FORMAT_MESSAGE_FROM_SYSTEM |
      FORMAT_MESSAGE_IGNORE_INSERTS,
      NULL,
      error,
      MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
      (LPWSTR)&lpMsgBuf,
      0, NULL);
    if (bufLen)
    {
      LPWSTR lpMsgStr = (LPWSTR)lpMsgBuf;
      std::wstring result(lpMsgStr, lpMsgStr + bufLen);

      LocalFree(lpMsgBuf);

      return convert_to_utf8(result);
    }
  }
  return std::string();
}

static std::wstring get_fullpath(const std::wstring& rel_path)
{
  static constexpr size_t max_str_len = 1024;
  wchar_t abs_path[max_str_len];
  WCHAR** lppPart = { NULL };

  if (GetFullPathName(rel_path.c_str(), max_str_len, abs_path, lppPart))
  {
    return std::wstring(abs_path);
  }

  return rel_path; // on failure return relative path
}

static std::wstring get_module_sourcepath(const Module& module)
{
  std::stringstream ss;
  ss << module.name << ".dll";
  return get_fullpath(convert_to_utf16(ss.str()));
}

static std::wstring get_module_lockpath(const Module& module)
{
  std::stringstream ss;
  ss << module.name << "_pdb.lock";
  return get_fullpath(convert_to_utf16(ss.str()));
}

static std::wstring get_module_copypath(const Module& module, int revision_delta = 0)
{
  std::stringstream ss;
  ss << module.name << "-" << (module.loaded_revision + revision_delta) << ".dll";
  return get_fullpath(convert_to_utf16(ss.str()));
}

static FILETIME get_file_last_write(const std::wstring& str)
{
  FILETIME creation_time;
  FILETIME access_time;
  FILETIME write_time;

  memset(&write_time, 0, sizeof(FILETIME));
  HANDLE file_handle = CreateFile(str.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
  if (file_handle != INVALID_HANDLE_VALUE)
  {
    // TODO : handle errors
    GetFileTime(file_handle, &creation_time, &access_time, &write_time);
    CloseHandle(file_handle);
  }

  return write_time;
}

int compare_filetime(const FILETIME& t1, const FILETIME& t2)
{
  if (t1.dwHighDateTime == t2.dwHighDateTime)
    return t1.dwLowDateTime - t2.dwLowDateTime;
  else
    return t1.dwHighDateTime - t2.dwHighDateTime;
}

static bool load_module_internal(Module& module, bool reloading)
{
  if (!reloading && module.loaded)
  {
    std::cerr << "failed to load module " << module.name << ": module is already loaded" << std::endl;
    return false;
  }

  // convert from multibyte
  std::wstring source_dll = get_module_sourcepath(module);
  std::wstring copy_dll = get_module_copypath(module, reloading ? 1 : 0);

  if (!CopyFile(source_dll.c_str(), copy_dll.c_str(), false))
  {
    std::cerr << "failed to load module " << module.name << ": can't copy the dll. " << get_last_error_string() << std::endl;
    return false;
  }

  // get load function
  HMODULE native_module_handle = LoadLibrary(copy_dll.c_str()); //GetModuleHandle(module_name_wide.c_str());
  if (!native_module_handle)
  {
    std::cerr << "failed to load module " << module.name << ": can't load the dll" << get_last_error_string() << std::endl;
    return false;
  }

  LoadFn load_function = (LoadFn)GetProcAddress(native_module_handle, "load_module");

  // load module
  if (!load_function)
  {
    std::cerr << "failed to load module " << module.name << " can't find the required load function (load_module) " << get_last_error_string() << std::endl;
    return false;
  }

  module.loaded = load_function(get_api_registry(), reloading);
  module.loaded_filetime = get_file_last_write(source_dll);

  if (!module.loaded)
  {
    std::cerr << "failed to unload module " << module.name << " returned an error while unloading" << std::endl;
  }

  return module.loaded;
}

static bool unload_module_internal(Module& module, bool reloading)
{
  if (!module.loaded)
  {
    std::cerr << "failed to unload module " << module.name << ": module is not loaded" << std::endl;
    return false;
  }

  // convert from multibyte
  std::wstring copy_dll = get_module_copypath(module, reloading ? -1 : 0);

  HMODULE native_module_handle = GetModuleHandle(copy_dll.c_str());
  if (!native_module_handle)
  {
    std::cerr << "failed to unload module " << module.name << ": can't get the module handle " << get_last_error_string() << std::endl;
    return false;
  }

  UnLoadFn unload_function = (LoadFn)GetProcAddress(native_module_handle, "unload_module");

  // load module
  if (!unload_function)
  {
    std::cerr << "failed to unload module " << module.name << " can't find the required unload function (unload_module) " << get_last_error_string() << std::endl;
    return false;
  }

  if (unload_function(get_api_registry(), reloading))
  {
    module.loaded = reloading || false;
    // TODO : check for possible errors
    if (FreeLibrary(native_module_handle))
    {
      DeleteFile(copy_dll.c_str());
    }
  }
  else
  {
    std::cerr << "failed to unload module " << module.name << " returned an error while unloading" << std::endl;
  }

  return true;
}

static bool update_module_internal(Module& module, bool reloading)
{
  if (!module.loaded)
    return true;

  std::wstring sourcedll_path = get_module_sourcepath(module);

  FILETIME writetime = get_file_last_write(sourcedll_path);
  if (compare_filetime(module.loaded_filetime, writetime) != 0)
  {
    std::wstring lockfile_path = get_module_lockpath(module);
    WIN32_FIND_DATA find_data;
    if (FindFirstFile(lockfile_path.c_str(), &find_data) != INVALID_HANDLE_VALUE)
      return true; // file is still locked, wait

    // update module
    std::cout << "module " << module.name << " needs to be reloaded" << std::endl;
    if (load_module_internal(module, true))
    {
      module.loaded_revision++;
      return unload_module_internal(module, true);
    }
    else
    {
      std::cerr << "failed to reload module" << module.name << std::endl;
      return false;
    }
  }

  return true;
}

bool add_module(const std::string& module_name)
{
  auto it = std::find_if(modules.begin(), modules.end(), [&module_name](Module& module) {return module.name == module_name; });
  if (it != modules.end())
  {
    std::cerr << "failed to add module " << module_name << "a module with the same name was alredy added." << std::endl;
    return false;
  }

  // TODO : clean previous copy of this library

  Module module;
  module.name = module_name;
  module.loaded_revision = 0;
  module.loaded = false;
  modules.push_back(module);
  return true;
}

bool load_modules()
{
  for (auto& module : modules)
  {
    if (!load_module_internal(module, false))
      return false; // fail on first module that fails to load.
  }

  return true;
}

bool unload_modules()
{
  for (auto it = modules.rbegin(); it != modules.rend(); ++it)
  {
    if (!unload_module_internal(*it, false))
      return false; // fail on first module that fails to unload.
  }

  return true;
}

bool update_modules()
{
  for (auto& module : modules)
  {
    if (!update_module_internal(module, false))
      return false; // fail on first module that fails to update.
  }
  return true;
}
