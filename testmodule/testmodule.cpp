// testmodule.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"

#include <iostream>

#include <common/api_registry.h>
#include <common/testmodule.h>

#pragma once

int return42()
{
  return 51; // OMG a bug (use hot reload to fix this)
}

static struct testmodule_i module_interface = {
  "Cpp test module",
  &return42
};

BEGIN_C

DLLEXPORT int load_module(struct api_registry_i *reg, int reload)
{
  std::cout << "testmodule is being loaded" << std::endl;
  reg->add(TESTMODULE_API_NAME, (void*)(&module_interface));
  return 1;
}

DLLEXPORT int unload_module(struct api_registry_i *reg, int reload)
{
  std::cout << "testmodule is being unloaded" << std::endl;
  reg->remove((void*)(&module_interface));
  return 1;
}

END_C
