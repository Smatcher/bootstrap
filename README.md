# Bootstrap

This repository serves as a playground for testing reloadable modules and the Vulkan API.

Programming style and concepts inspired by blog posts at [ourmachinery](http://ourmachinery.com/).

See also [vulkan-tutorial](https://vulkan-tutorial.com/).

### Prerequisites:

* Visual studio 2017
* Python 3.6 (x64) installed and in the environment path
* CFFI Python module (pip install cffi)

### Known Issues:

* Python plugin doesn't rebuild automatically, you have to call rebuild project manually.

