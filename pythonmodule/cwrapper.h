#include <common/utils.h>
#include <common/api_registry.h>
#include <common/testmodule.h>

#include <stdio.h>

// these functions are implemented in python
static int myfunction();

// declare the module interface
static struct testmodule_i module_interface = {
  "Python test module",
  &myfunction
};

// load/unload functions
CFFI_DLLEXPORT int load_module(struct api_registry_i* reg, int reload)
{
  printf("python_module is being loaded\n");
  reg->add(TESTMODULE_API_NAME, (void*)(&module_interface));
  return 1;
}

CFFI_DLLEXPORT int unload_module(struct api_registry_i* reg, int reload)
{
  printf("python_module is being unloaded\n");
  reg->remove((void*)(&module_interface));
  return 1;
}
