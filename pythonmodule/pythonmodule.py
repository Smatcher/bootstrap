
def build_library():
    import cffi
    ffibuilder = cffi.FFI()
    
    ffibuilder.set_source("my_plugin", r"""#include <pythonmodule/cwrapper.h>""", include_dirs=["../.."])

    ffibuilder.cdef("""
        extern "Python" {
            int myfunction();
        }
    """)

    ffibuilder.embedding_init_code("""
        from my_plugin import ffi
        import sample_pyfile

        print("Running some python from a py file {0}".format(sample_pyfile.getSampleText()))

        @ffi.def_extern()
        def myfunction():
            return 42
    """)

    ffibuilder.compile(target="python_plugin.*", verbose=True)

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='Python module for the bootstrap example.')
    parser.add_argument('--build', dest='build', action='store_true', help='build a native dll for the module')
    args = parser.parse_args()
    if args.build is not None:
        build_library()
    else:
        parser.print_help()
