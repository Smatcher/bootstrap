#pragma once

#ifdef __INTELLISENSE__
#include "utils.h"
#endif

BEGIN_C

struct api_registry_i
{
  void  (*add)(const char *name, void *interf);
  void  (*remove)(void *interf);
  void* (*first)(const char *name);
  void* (*next)(void *prev);
};

END_C
