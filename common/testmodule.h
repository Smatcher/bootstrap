#pragma once

#ifdef __INTELLISENSE__
#include "utils.h"
#endif

BEGIN_C

#define TESTMODULE_API_NAME "testmodule_i"

struct testmodule_i
{
  const char* name;
  int(*return_something)();
};

END_C
