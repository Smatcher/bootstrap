#pragma once

#ifndef DLLEXPORT
#  ifdef _WIN32
#    define DLLEXPORT extern __declspec(dllexport)
#  else
#    define DLLEXPORT extern
#  endif
#endif

#ifdef __cplusplus
#  define BEGIN_C extern "C" {
#  define END_C }
#else
#  define BEGIN_C
#  define END_C
#endif
