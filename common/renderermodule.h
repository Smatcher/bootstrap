#pragma once

#ifdef __INTELLISENSE__
#include "utils.h"
#endif

BEGIN_C

#define RENDERERMODULE_API_NAME "renderermodule_i"

struct renderermodule_i
{
	const char* name;
	int(*init)();
	int(*loop)();
	int(*cleanup)();

	void* state;
};

END_C

